﻿var TP = require("touch");
var transitions = require("transitions");
var WX = require("modules/wx");
var storageInfo = require("modules/storageInfo");
var loading = require("modules/loading");

Page({
  exitFlg: false,
  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) {
    loading.show(this, 80, "@waitqrcode");
    loading.timeout(this);
    WX.SyncPageObject("login", this);

    storageInfo.clearLoginCache();
    WX.GetLoginURL(this)
  },

  /* 页面显示时触发该函数 */
  onShow: function (event) { },

  setStatus: function (stat) {
    this.setData({ label1: ("@" + stat) })
  },

  loginFail: function () {
    loading.hide(this);
    loading.show(this, 80, "@wxscancode");
    loading.timeout(this);
    WX.GetLoginURL(this)
  },

  loginSucc: function () {
    loading.hide(this);
    setTimeout(function () {
      transitions.appOrPageEnterAnim(true);
      pm.redirectTo("pages/mainWindow/mainWindow")
    }, 1);
  },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
    loading.hide(this);
    this.exitFlg = true
    WX.SyncPageObject("login", null);
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0);
  },

});
