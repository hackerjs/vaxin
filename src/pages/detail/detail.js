﻿var TP = require("touch");
var transitions = require("transitions");

Page({
  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
    this.setData({ mtxt: { value: event, automatic: true, } })
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) { },

  /* 页面显示时触发该函数 */
  onShow: function (event) { },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0);
  },

});
