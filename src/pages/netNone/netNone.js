﻿var TP = require("touch");
var transitions = require("transitions");
var scroll = app.getInstance().data.scroll;

Page({
  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
    if (event == "loading") {
      this.setData({ MultiTextBox1: { automatic: true, value: "@loadErrCheckNet" }, label1: { scroll: scroll } });
    } else {
      this.setData({ MultiTextBox1: { automatic: true }, label1: { scroll: scroll } });
    }

    var that = this;
    this.closeTimer = setTimeout(function () {
      var pageNm = pm.getPageName();
      if (pageNm == "netNone") {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }
      clearTimeout(that.closeTimer);
    }, 1500);
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) { },

  /* 页面显示时触发该函数 */
  onShow: function (event) { },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
    clearTimeout(this.closeTimer);
  },

  onBtn: function (event) {
    transitions.appOrPageEnterAnim();
    pm.navigateBack();
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0
    );
  },
});
