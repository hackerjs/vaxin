﻿var TP = require("touch");
var transitions = require("transitions");
var WX = require("modules/wx");
var scroll = app.getInstance().data.scroll;
var network = require('system.network');
var loading = require("modules/loading");

Page({
  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) {
    loading.show(this, 80);
    loading.timeout(this);
    this.setData({ listctrl1: { empty: true } })

    var that = this
    localStorage.onChange(this.netChange_obj = {
      key: "wx.nettype",
      callback: function (lsargs) {
        var netType = lsargs.new_value;
        var getnteitem = that.getData("notnetitem", "rect")
        if (netType == "none") {
          if (!getnteitem) {
            that.setData({
              listctrl1: {
                list: {
                  page: that,
                  items: [{
                    xml: "panels/notnet",
                    index: 0,
                    items: [{ notnet: { id: "notnetitem" } }]
                  }]
                }
              }
            })
          }
        } else {
          if (getnteitem) {
            that.setData({ listctrl1: { remove: "notnetitem" } })
          }
        }
      }
    });

    this.netChange_obj.callback({ new_value: localStorage.getItem("wx.nettype") })
    WX.SetAppActive(true)
    WX.SyncPageObject("main", this);
    WX.initWechat();
  },

  reLogin: function () {
    this.setData({ listctrl1: { empty: true } })
    WX.SetAppActive(false)
    setTimeout(function () {
      WX.ResetGlobalVar()
      localStorage.setItem("wx.servered", 0, "com.github.wx")
      transitions.appOrPageEnterAnim();
      pm.redirectTo("pages/login/login")
    }, 1)
  },

  setNickName: function (nickname) {
    this.setData({ lblname: { value: nickname, scroll: scroll, } })
  },

  syncView: function () {
    this.setData({ listctrl1: { empty: true } })
    var contcatList = []
    loading.hide(this);

    // console.log(console.backtrace());
    // console.log("\r\n\r\n getcontact all data ", getContact, "\r\n\r\n")
    var getContact = WX.GetChatItems()
    for (var index = 0; index < getContact.length; index++) {
      var element = getContact[index];

      // var msgInfo = element.message
      var timeStr = ""
      if (element.time) {
        timeStr = new Date(element.time).toString().slice(16, 24)
      }

      var lastmsg = (element.lastmessage ? element.lastmessage : "@nomsgyet")
      var itmObj = {
        chatItem: { id: "chatItem" + element.userid },
        panel1: { layout: "hcbox" },
        label1: { id: ("label1" + element.userid), hide: (element.count == 0), value: element.count, scroll: scroll, },
        label2: { id: ("label2" + element.userid), value: element.name, scroll: scroll, },
        label3: { id: ("label3" + element.userid), maxrows: 1, value: lastmsg, /*scroll: scroll,*/ },
        label4: { id: ("label4" + element.userid), value: timeStr, /*scroll: scroll,*/ },
        imgboxBg: { id: ("imgboxBg" + element.userid), },
        PanelPlus1: {
          id: "PanelPlus1" + element.userid,
          button: {
            enable: true,
            anim: { enable: false, pro: 0.96 },
            callback: this.onPplsTouch,
          },
        },
      }
      contcatList.push(itmObj)
    }

    this.setData({
      listctrl1: {
        list: {
          page: this,
          items: [{
            xml: "panels/chatItem",
            items: contcatList
          }]
        }
      }
    })
  },

  onPplsTouch: function (event) {
    if (event.type && event.type == "touchend") {
      var that = this;
      var id = event.target.id.substr("PanelPlus1".length);

      var obj = {};
      obj["imgboxBg" + id] = { hide: false };
      this.setData(obj);

      if (network.getTypeSync().type == "none") {
        transitions.appOrPageEnterAnim(true);
        pm.navigateTo("pages/netNone/netNone");
        var obj = {};
        obj["imgboxBg" + id] = { hide: true };
        that.setData(obj);
        return;
      }

      // this.setData({ imgTouchBg: { hide: false } });
      this.laterTimer = setTimeout(function () {
        clearTimeout(that.laterTimer);
        var obj = null;
        var returnObj = {};
        var items = WX.GetChatItems()
        for (var idx = 0; idx < items.length; idx++) {
          obj = items[idx];
          if (obj.userid == id) {
            items[idx].count = 0
            console.log(id + "=============1") //, obj);
            returnObj = {
              name: obj.name,
              id: id,
              message: obj.message,
            };
            break;
          }
        }

        console.log("#>> chat item click " + id, returnObj);
        if (!returnObj.name) {
          var sobj = {};
          sobj["imgboxBg" + id] = { hide: true };
          // sobj["label1" + id] = { hide: true, value: "0" };
          that.setData(sobj);
          console.log("#>>  chat item lists ", items);
          return
        }

        transitions.appOrPageEnterAnim(true);
        pm.navigateTo({ url: "pages/chat/chat", value: returnObj })
        var sobj = {};
        sobj["imgboxBg" + id] = { hide: true };
        sobj["label1" + id] = { hide: true, value: "0" };
        that.setData(sobj);
      }, 100);
    }
  },

  onCard: function (event) {
    if (event.type == "change") {
      this.setData({ dot: event.detail.value });
    } else if (event.type == "dropFirst") {
      var exitapp = setTimeout(function () {
        clearTimeout(exitapp)
        transitions.applist2App();
        console.log("#>> backgrounder: " + app.getInstance().data.backgrounder);
        if (app.getInstance().data.backgrounder) {
          app.hide()
        } else {
          pm.navigateBack()
        }
      }, 200);
    }
  },

  onUpdate: function (event) {
    if (!event) {
      this.syncView()
    }
  },

  exitApp: function () {
    WX.SetAppActive(false)
    transitions.applist2App();
    app.exit()
  },

  /* 页面显示时触发该函数 */
  onShow: function (event) {
    console.log("#>> AppActive: " + WX.GetAppActive());
    if (WX.GetAppActive() == false) {
      WX.SetAppActive(true)
      WX.SyncCheck()
    }
  },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
    localStorage.offChange(this.netChange_obj)
    loading.hide(this);
    WX.SyncPageObject("main", null);
    WX.SetAppActive(false)
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0);
  },

});
