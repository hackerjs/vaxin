﻿var TP = require("touch");
var transitions = require("transitions");
var WX = require("modules/wx");
var network = require('system.network');
var toastDisplay = require("modules/toastDisplay");

Page({
  toUserName: null,
  quickReply: ["好。", "可以。", "不行。", "等下回复你。",],
  quickLastTime: null,

  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
    this.toUserName = event.id
    this.setData({ title: event.name, listctrl1: { hide: false }, listctrl2: { hide: true } })
    this.syncView(event.message)
    WX.SyncPageObject("chat", this);
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) {
    var arr = []
    for (var idx = 0; idx < this.quickReply.length; idx++) {
      var element = this.quickReply[idx];
      arr.push({ button1: { detail: { text: element }, value: element, }, })
    }
    arr.push({ button1: { hide: true } })

    this.setData({
      listctrl2: {
        list: {
          page: this,
          items: [{
            xml: "panels/quickItem",
            items: arr,
          }]
        }
      }
    })
  },

  /* 页面显示时触发该函数 */
  onShow: function (event) { },

  syncView: function (messages) {
    if (!messages) {
      console.log("#>> in chat syncView, message is undefined, return");
      return
    }
    this.setData({ listctrl1: { empty: true } })
    var contcatList = []
    for (var index = 0; index < messages.length; index++) {
      var element = messages[index];
      var content = element.content
      if (content === null || content === undefined) {
        content = ""
      }

      var len = content.replace(/[^\u0000-\u00FF]/g, '**').length
      // if (len > 60) {
      //   content = content.slice(0, 57) + "..."
      // }
      var recetype = (element.msgtype == "receive")
      var sendtype = (element.msgtype == "send")
      var itmObj = {
        messageItem: { id: ("messageItem" + element.msgid), },
        imagebox1: { visible: recetype },
        imagebox2: { visible: sendtype },
        mtxt: { visible: recetype, value: content, maxrows: 2, background: 0xFFFFFFFF },
        mtxtsend: { visible: sendtype, value: content, maxrows: 2, background: 0xFF95EC69 },
        button1: { id: ("button" + element.msgid), detail: { len: len, text: element.content, } },
      }
      contcatList.push(itmObj)
    }

    this.setData({
      listctrl1: {
        list: {
          page: this,
          items: [{
            xml: "panels/messageItem",
            items: contcatList
          }]
        }
      }
    })
  },

  MsgItemClick: function (event) {
    if (event.detail && event.detail.len > 38) {
      pm.navigateTo({ url: "pages/detail/detail", value: event.detail.text, })
    }
  },

  quickClick: function (event) {
    if (network.getTypeSync().type == "none") {
      transitions.appOrPageEnterAnim(true);
      pm.navigateTo("pages/netNone/netNone");
      return;
    }

    if ((!this.quickLastTime) || (new Date().getTime() - this.quickLastTime > 800)) {
      var content = event.detail.text
      console.log("#>>event.detail.text: " + content);
      this.quickLastTime = new Date().getTime()
      // WX.SendMsg(content, this.toUserName, sendid);
      this.sendMessage(content, true);
      this.closeQR()
    }
  },

  openQR: function () {
    this.setData({ listctrl1: { move: 30000, } });
    this.setData({ listctrl2: { hide: false, }, btnopenQR: { hide: true } });
  },

  closeQR: function () {
    this.setData({ listctrl2: { hide: true, }, btnopenQR: { hide: false } });
  },

  onVoice: function (params) {
    if (app.getInstance().data.VoiceKey == "" || app.getInstance().data.VoiceSecret == "") {
      toastDisplay.show("@configvoicekey");
      return
    }

    transitions.appOrPageEnterAnim(true);
    pm.navigateTo("pages/recordAudio/recordAudio")
  },

  onEmo: function (params) {
    transitions.appOrPageEnterAnim(true);
    pm.navigateTo("pages/emo/emo")
  },

  onUpdate: function (event) {
    if (event) {
      // WX.SendMsg(event.text, this.toUserName);
      this.sendMessage(event.text, true);
      this.closeQR()
    }
  },

  sendMessage: function (ctxt, send) {
    var sendid = ("" + new Date().getTime())
    var len = ctxt.replace(/[^\u0000-\u00FF]/g, '**').length
    this.setData({
      listctrl1: {
        list: {
          page: this,
          items: [{
            xml: "panels/messageItem",
            items: [{
              messageItem: { id: ("messageItem" + sendid), },
              imagebox1: { visible: !send },
              imagebox2: { visible: !!send },
              mtxt: { visible: !send, value: ctxt, maxrows: 2, background: 0xFFFFFFFF },
              mtxtsend: { visible: !!send, value: ctxt, maxrows: 2, background: 0xFF95EC69 },
              button1: { id: ("button" + sendid), detail: { len: len, text: ctxt, } },
              aniimg: { id: ("aniimg" + sendid), hide: !send, value: "start" },
            }]
          }]
        }
      }
    })

    this.setData({ listctrl1: { move: 30000, } });

    if (send) {
      WX.SendMsg(ctxt, this.toUserName, sendid);
    }
  },

  sendSuccId: function (id) {
    var ctrl = this.getData(("aniimg" + id), "hide")
    if (ctrl == false) {
      var idobj = {}
      idobj[("aniimg" + id)] = { hide: true }
      this.setData(idobj)
    }
  },

  appendMsg: function (msgItem) {
    this.sendMessage(msgItem.content)
  },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
    WX.SyncPageObject("chat", null);
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0);
  },

});
