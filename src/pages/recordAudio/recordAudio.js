﻿var TP = require("touch");
var transitions = require("transitions");
var record = require("system.record")
var toastDisplay = require("modules/toastDisplay");
var FileSystemManager = pm.getFileSystemManager()
var DATA = app.getInstance().data;
var rpc = require('system.mobileRPC');

Page({
  pageData: {
    histories: [],
    recoding: false,
    recodeTimer: null,
    loadRecordCount: 0,
    playingId: null,
    playStatus: false,
    dialogCounts: 0,
    recordMaxSecond: 8,
    recordName: null,     // 录音文件名
  },
  TextValue: null,
  CanNextPage: false,
  /* 页面加载时触发该函数 */
  onLoad: function (event) {
    TP.PageTouchInit(this);
    this.setData({ resultPanel: { hide: true }, })
  },

  /* 此方法展示窗体后发生 */
  onResume: function (event) { },

  /* 页面显示时触发该函数 */
  onShow: function (event) { },

  /* 页面隐藏时触发该函数 */
  onHide: function (event) { },

  /* 页面退出时触发该函数 */
  onExit: function (event) {
    TP.PageTouchUninit(this);
    this.pageData.recodeTimer && clearInterval(this.pageData.recodeTimer)
    this.stopRecordVoice()
    this.exitFlg = true
  },

  onPageTouch: function (event) {
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        transitions.appOrPageEnterAnim();
        pm.navigateBack();
      }, 0, 0);
  },

  // onMenuPanelTouch: function (event) { },

  onResultPanelTouch: function (event) {
    var that = this
    TP.PageTouchEvent(this, event, 0, 0,
      function () {
        that.setData({ resultPanel: { hide: true } });
      }, 0, 0);
  },

  onRecord: function () {
    var that = this
    this.CanNextPage = false

    if (false && this.pageData.dialogCounts > 5) {
      toastDisplay.show("@morethanmax")
      return
    }

    console.log("#>> onRecord: " + this.pageData.recoding);
    this.pageData.recodeTimer && clearInterval(this.pageData.recodeTimer)
    if (this.pageData.recoding) {
      this.stopRecordVoice()
    } else {
      this.setData({
        // panelpop: { hide: false },
        animrecord1: { value: "start" },
        animrecord2: { value: "start" },
        MultiTextBox1: "",
      })
      this.pageData.recordName = (new Date().getTime()) + '.wav'
      var times = this.pageData.recordMaxSecond
      this.record_object = {
        uri: 'internal://files#voice/' + this.pageData.recordName,
        mode: "file",
        format: "wav",
        callback: function (event) {
          console.log("#>>record_object------>" + that.pageData.recordName + "    " + JSON.stringify(event));
          if (event.event == "end") {
            that.pageData.dialogCounts++

            // reset UI & flag -- start
            that.pageData.recoding = false
            that.pageData.recodeTimer && clearInterval(that.pageData.recodeTimer)
            times = that.pageData.recordMaxSecond
            that.setData({
              animrecord1: { value: "stop" },
              animrecord2: { value: "stop" },
              lblsecond: (times + "s"),
            })
            // that.setData({ panelpop: { hide: true } });
            // reset UI & flag -- end

            if (that.CanNextPage) {
              // return
            }
            that.setData({ resultPanel: { hide: false } });
            that.execRpc(that.pageData.recordName)
            // pm.navigateTo({ url: "pages/parseAudio/parseAudio", value: { audioPath: that.pageData.recordName } })
          }
        }
      }
      this.pageData.recoding = true
      record.start(this.record_object)

      this.pageData.recodeTimer = setInterval(function () {
        times--
        if (times == 0) {
          that.pageData.recodeTimer && clearInterval(that.pageData.recodeTimer)
          that.stopRecordVoice()
        } else {
          if (times < 4) {
            var val = toastDisplay.getValueByKey("@recordstop2").replace("{0}", times)
            that.setData({ mtxtrecoedtip: val })
          }
          that.setData({ lblsecond: (times + "s") })
        }
      }, 1000)
    }
  },

  stopRecordVoice: function () {
    this.CanNextPage = true;
    this.setData({ resultPanel: { hide: false }, label2: { value: "@audioparse" } });
    this.resetUI();
  },

  resetUI: function () {
    console.log("#>> resetui in");
    record.stop(this.record_object)
    this.setData({ /*panelpop: { hide: true, },*/ mtxtrecoedtip: "@recordstop1" });
    // resultPanel
  },

  getContentByUrl: function (url) {
    var readSongObj = FileSystemManager.readFileSync({ filePath: url, });
    var result = new Uint8Array(readSongObj)
    var encodedString = String.fromCharCode.apply(null, result);
    var gettxt = decodeURIComponent(escape(encodedString));
    console.log(url + "  #>> gettxt: " + pm.clock(), gettxt);
    return gettxt
  },

  execRpc: function (locaPath) {
    // var savelong = new Date().getTime()
    var that = this
    console.log("#>> execRpc userurl: " + locaPath)

    function failFunc(err) {
      console.log("parseaudio faile=>>> ", err);
      if (err.code == -2) {
        toastDisplay.show("@connphone")
      } else {
        toastDisplay.show("@getfail")
      }
      that.setData({ resultPanel: { hide: true }, /*button3: { hide: false },*/ })
    }

    // 注意事项：ERNIE-Bot 4.0 QPS为 - https://console.bce.baidu.com/qianfan/ais/console/onlineService?tab=preset
    rpc.execRpc({
      name: "exec_py_patch",
      // 输入是字符串
      params: JSON.stringify({
        plugin: (DATA.appAbsltDataPath + "/audio2Text.py"),
        plcfile: (DATA.appAbsltDataPath + "/voice/" + locaPath),
        Keys: {
          VoiceKey: DATA.VoiceKey,
          VoiceSecret: DATA.VoiceSecret,
        }
      }),
      success: function (res) {
        var data = res.data
        var getObj = JSON.parse(data)
        if (getObj.code && getObj.code == -1) {
          failFunc({ code: -1, data: getObj.data })
          return
        }
        console.log("#>>  get text", getObj)
        if (getObj.text) {
          // that.onResponseWord({ user: getObj.text })
          that.setData({ MultiTextBox1: { value: getObj.text, automatic: true, }, label2: { value: "" } })
          that.TextValue = getObj.text
        }
        // that.setData({ resultPanel: { hide: true }, /*button3: { hide: false },*/ })
      },
      fail: failFunc
    });
  },

  onReload: function () {
    this.setData({ resultPanel: { hide: true }, })
  },

  onSend: function () {
    transitions.appOrPageEnterAnim();
    pm.navigateBack({ text: this.TextValue })
  },

});
