var toastDisplay = require("modules/toastDisplay");
var request = require('system.request');
var FileSystemManager = pm.getFileSystemManager();
var storageInfo = require("modules/storageInfo");

module.exports = {
  // ----------- down1oadImage Set img start ----
  // url, abspath, uriname, id, scale, radius, 
  downloadImage2Set: function (obj, callback, page) {
    var addUrl = obj.url;
    if (!addUrl) {
      console.log("img download url is undefined: [" + addUrl + "], call callback, then return.");
      callback && callback();
      return;
    }
    var that = page;
    // console.log("down1oadImage2Set img download func full url >> " + addUrl) //, obj);

    if (storageInfo.getFreeStorage(null, "image")) {
      console.log("#>> download songs Covers callback no storage.");
      return;
    }
    this.downImage1 = {
      url: addUrl,
      filename: obj.abspath,
      callback: function (data) {
        if (data.type == 0) {  // success
          console.log("#>> down1oadImage2Set img download success: " + obj.uriname + "  exitFlg:" + that.exitFlg);
          if (that.exitFlg) {
            return;
          }
          var getFileInfoSync = FileSystemManager.getFileInfoSync({ filePath: obj.uriname, });
          console.log("#>> down1oadImage2Set getFileInfoSync: " + JSON.stringify(getFileInfoSync));
          if (getFileInfoSync && getFileInfoSync > 0) {
            var objId = {};
            objId[obj.id] = { value: obj.uriname, scale: obj.scale, borderRadius: obj.radius };
            that.setData(objId);
            console.log("#>> will down succ callback", objId);
            obj.downloadSuccCallback && obj.downloadSuccCallback()
          } else {
            console.log("#>> down1oadImage2Set image [" + obj.uriname + "] down succ, but size is " + getFileInfoSync);
            toastDisplay.show("@storageEmpty");
          }
        }
      },
      fail: function (res) {
        console.log("#>> down1oadImage2Set img download fail..." + JSON.stringify(res));
        if (that.exitFlg) {
          return;
        }
        var unlinkSync = FileSystemManager.unlinkSync({
          filePath: obj.abspath,
        });
        that.loginFail()
      },
    }

    callback && callback();
    var localNoExist = FileSystemManager.accessSync({ path: obj.uriname });
    // console.log("#> down1oadImage2Set localNoExist: ", localNoExist);
    if (localNoExist || obj.download) {
      console.log("#>> down1oadImage2Set will 2 download." + obj.uriname);
      request.download(this.downImage1);
    } else {
      console.log("#>> down1oadImage2Set localFile exist, set." + obj.uriname);
      this.downImage1.callback({ type: 0 });
    }
  },
  // ----------- down1oadImage Set img end ----
}