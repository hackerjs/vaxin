var FileSystemManager = pm.getFileSystemManager();

module.exports = {
  // 根据传入文件大小，比对剩余存储。如果未传入文件大小参数则返回实际容量
  getFreeStorage: function (fileSize, downType) {
    var FileSystemManager = pm.getFileSystemManager();
    // 读取目录下存储 internal:///#com.github.wx
    var statFsSync = FileSystemManager.statFsSync({
      path: app.getInstance().data.appDataURIPath,
    });
    if (statFsSync && statFsSync.bsize) {
      var freeSize = statFsSync.bsize * statFsSync.bfree;
      console.log("#>> statFsSync succ. total size: " + (statFsSync.bsize * statFsSync.blocks) + "; free size: " + freeSize + "; para fileSize: " + fileSize + " type: " + downType);
      var lessLimit = false;
      switch (downType) {
        case "image":
          // 1. 图片 1M 清理，不提示
          lessLimit = freeSize < (1024 * 1024);
          break;
        case "mp3":
          // 2. MP3 下载 10M，提示
          lessLimit = freeSize < (1024 * 1024 * 10);
          break;
      }
      return lessLimit;
    } else {
      console.log("#>> statFsSync fail. Msg: ", statFsSync);
      if (fileSize == undefined) {
        return 0;
      } else {
        // 获取信息失败
        return true;
      }
    }
  },

  clearLoginCache: function () {
    var appDataURIPath = app.getInstance().data.appDataURIPath;
    var readdirSync = FileSystemManager.readdirSync({ dirPath: appDataURIPath + "/images/login", });
    console.log(" # > readImsges, dir: login, files: ", readdirSync);
    for (var idx = 0; idx < readdirSync.length; idx++) {
      FileSystemManager.unlinkSync({
        filePath: (appDataURIPath + "/images/login/" + readdirSync[idx]),
      });
    }
  },

  FileNotExist: function (path) {
    var localNoExist = FileSystemManager.accessSync({ path: path });
    return !(!!localNoExist);
  },

}