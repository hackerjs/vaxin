var fetch = require('system.fetch');
var download = require("modules/download");

var FileSystemManager = pm.getFileSystemManager();

var PATH_URI = app.getInstance().data.appDataURIPath
var PATH_ABS = app.getInstance().data.appAbsltDataPath

var MEDIAURL = PATH_URI + "/media/"
var loading = require("modules/loading");
var notification = require("system.notification");

var GETWECHATLIST = []

var UUID = null
var DOMAIN = "wx.qq.com"
var COOKIES = []
var USERNAME = ""
var DEVICEID = getDeviceID()
var SYNCKEY = {}
var SYNCCHECKKEY = {}
var GROUPIDANDMEMBERLIST = {}
var CANEXECSYNC = true

var LOCAL_SKEY_ = ""
var LOCAL_WXSID = ""
var LOCAL_WXUIN = ""
var LOCAL_PSTKT = ""
var LOOPGETGROUPIDS = []
var PAGEOBJECT = {}
var APPACTIVE = true

function ResetGlobalVar() {
  GETWECHATLIST = []
  UUID = null
  DOMAIN = "wx.qq.com"
  COOKIES = []
  USERNAME = ""
  // DEVICEID = getDeviceID()
  SYNCKEY = {}
  SYNCCHECKKEY = {}
  GROUPIDANDMEMBERLIST = {}
  CANEXECSYNC = true

  LOCAL_SKEY_ = ""
  LOCAL_WXSID = ""
  LOCAL_WXUIN = ""
  LOCAL_PSTKT = ""

  LOOPGETGROUPIDS = []
  PAGEOBJECT = {}
}

function initialConfig() {
  DOMAIN = localStorage.getItem("wx.domain");
  COOKIES = localStorage.getItem("wx.cookie");
  USERNAME = localStorage.getItem("wx.username");
  DEVICEID = localStorage.getItem("wx.deviceid");
  SYNCKEY = localStorage.getItem("wx.synckey");

  LOCAL_SKEY_ = localStorage.getItem("wx.skey")
  LOCAL_WXSID = localStorage.getItem("wx.wxsid")
  LOCAL_WXUIN = localStorage.getItem("wx.wxuin")
  LOCAL_PSTKT = localStorage.getItem("wx.pass_ticket")

  CANEXECSYNC = true
}

function uninitialConfig() {
  CANEXECSYNC = false
}

function getDeviceID() {
  var saveid = localStorage.getItem("wx.deviceid")
  if (!saveid) {
    var builder = "e";
    for (var i = 0; i < 15; i++) {
      var r = Math.floor(Math.random() * 10);
      builder += r;
    }
    saveid = builder
    localStorage.setItem("wx.deviceid", builder, "com.github.wx")
  }
  console.log("#>> DEVICEID: " + DEVICEID)
  return saveid
}

function getRequest(obj) {
  console.log("\r\n\r\n\r\n#>> new request\r\naaaaaaaaaaaa")

  var fetch_get_cb_arg = {
    url: obj.url,
    method: "GET",
    timeout: 30 * 1000,
    responseType: "json",
    success: function (data) {
      console.log("\n#>> getRequest fetch succ url: " + (obj.url) + "  return data.code: [" + data.code + "]");
      if (data && data.code != 200) {
        console.log("\r\n#>> getReque5t fetch succ > fail ", data.data);
        obj.fail && obj.fail(data);
      } else {
        obj.success && obj.success(data);
      }
    },
    fail: function (err) {
      console.log("\n#>> getRequest fetch fail url: " + (obj.url) + "  return data.code: [" + err.code + "]");
      obj.fail && obj.fail(err);
    },
    complete: function () {
      obj.complete && obj.complete();
    }
  }

  if (obj.params) {
    fetch_get_cb_arg.params = obj.params;
  }
  if (obj.header) {
    fetch_get_cb_arg.header = obj.header;
  }
  if (obj.responseType) {
    fetch_get_cb_arg.responseType = obj.responseType
  }

  if (!true) {
    console.log("#>> get request object", fetch_get_cb_arg);
  }

  fetch.fetch(fetch_get_cb_arg);
}

function defaultPostBaseRequest() {
  // var addPublicData = ["wxuin", "wxsid", "skey",]
  // var addPublicKey = ["Uin", "Sid", "Skey",]
  var BaseRequest = {}
  // for (var index = 0; index < addPublicData.length; index++) {
  //   var element = addPublicData[index];
  //   BaseRequest[addPublicKey[index]] = localStorage.getItem("wx." + element)
  // }

  BaseRequest["Uin"] = LOCAL_WXUIN
  BaseRequest["Sid"] = LOCAL_WXSID
  BaseRequest["Skey"] = LOCAL_SKEY_
  BaseRequest["DeviceID"] = DEVICEID
  return BaseRequest
}

function postRequest(obj) {
  var BaseRequest = defaultPostBaseRequest()
  var fetch_get_cb_arg = {
    url: (obj.url),
    data: { BaseRequest: BaseRequest, },
    header: obj.header,
    method: "POST",
    success: function (data) {
      console.log("\n#>> p0stRequest fetch succ url: " + (obj.url) + "  return data.code: [" + data.code + "]");
      if (data.data && data.data.error_no) {
        console.log("\n#>> p0stRequest fetch succ > fail ", data.data);
        obj.fail && obj.fail(data);
        return
      }
      obj.success && obj.success(data);
    },
    fail: function (data) {
      console.log("\n#>> postRequest fail url: " + (obj.url) + "  return data.code: [" + data.code + "]");
      obj.fail && obj.fail(data);
    },
    complete: function () {
      obj.complete && obj.complete();
    }
  }

  if (obj.params) {
    fetch_get_cb_arg.params = obj.params
  }

  if (obj.data) {
    for (var key in obj.data) {
      var element = obj.data[key];
      fetch_get_cb_arg.data[key] = element
    }
  }

  if (obj.responseType) {
    fetch_get_cb_arg.responseType = obj.responseType
  }

  if (!true) {
    console.log("#>> post request object", fetch_get_cb_arg);
  }
  fetch.fetch(fetch_get_cb_arg);
}

function GetPageObj(pagename) {
  var pages = pm.getPagesName()
  // console.log("-----> pages", pages);
  if (pages[0] == pagename) {
    var findPage = pm.getPageObject()
    return findPage
  } else {
    return undefined
  }
}

function GetLoginURL(lognPage) {
  var url = 'https://login.wx.qq.com/jslogin'
  var paraObj = {
    redirect_uri: 'https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxnewloginpage?mod=desktop',
    appid: 'wx782c26e4c19acffb',
    fun: 'new',
    lang: 'zh_cn',
    _: new Date().getTime().toString(),
  }

  var header = {
    'User-Agent': 'Mozilla/5.0 (Linux; U; UOS x86_64; zh-cn) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 UOSBrowser/6.0.1.1001',
  }

  var params = jsonSort(paraObj);
  if (params) { url += "?" + params }

  var obj = {
    url: url,
    header: header,
    success: function (res) {
      if (res.code == 200) {
        UUID = GetValueBtKey(res.data, "window.QRLogin.uuid")
        var urlx = "https://login.weixin.qq.com/qrcode/" + UUID
        console.log(url, 'Open Browser Sacn')
        var tms = new Date().getTime()
        var file_end = "/images/login/login" + tms + ".jpg"
        var absAvatar = PATH_ABS + file_end
        var uriAvatar = PATH_URI + file_end

        function downloadsucc() {
          loading.hide(lognPage);
          getLogin_Status()
        }

        download.downloadImage2Set({
          url: urlx,
          uriname: uriAvatar,
          abspath: absAvatar,
          id: "loginimagebox1",
          scale: true,
          radius: 20,
          downloadSuccCallback: downloadsucc,
          download: true,
        }, null, lognPage)
      }
    },
    fail: function () {
      if (PAGEOBJECT["login"]) {
        PAGEOBJECT["login"].loginFail();
      }
    }
  }
  getRequest(obj, lognPage)
}

function getLogin_Status() {
  var url = 'https://login.wx.qq.com/cgi-bin/mmwebwx-bin/login';
  if (!PAGEOBJECT["login"]) {
    console.log("#>> login page closed, break login")
    return
  }
  var time = new Date().getTime() / 1000
  var paraObj = {
    r: (time / 1579).toFixed(0),
    _: time.toFixed(0),
    loginicon: true,
    uuid: UUID,
    tip: 0,
  }

  var params = jsonSort(paraObj)
  if (params) { url += "?" + params }

  var obj = {
    url: url,
    success: function (res) {
      if (res.code == 200) {
        var scandtstus = GetValueBtKey(res.data, "window.code")
        if (scandtstus == 200) {
          var redirecturl = GetValueBtKey(res.data, "window.redirect_uri")
          if (redirecturl) {
            var header = {
              "version": "2.0.0",
              "extspam": "Go8FCIkFEokFCggwMDAwMDAwMRAGGvAESySibk50w5Wb3uTl2c2h64jVVrV7gNs06GFlWplHQbY/5FfiO++1yH4ykCyNPWKXmco+wfQzK5R98D3so7rJ5LmGFvBLjGceleySrc3SOf2Pc1gVehzJgODeS0lDL3/I/0S2SSE98YgKleq6Uqx6ndTy9yaL9qFxJL7eiA/R3SEfTaW1SBoSITIu+EEkXff+Pv8NHOk7N57rcGk1w0ZzRrQDkXTOXFN2iHYIzAAZPIOY45Lsh+A4slpgnDiaOvRtlQYCt97nmPLuTipOJ8Qc5pM7ZsOsAPPrCQL7nK0I7aPrFDF0q4ziUUKettzW8MrAaiVfmbD1/VkmLNVqqZVvBCtRblXb5FHmtS8FxnqCzYP4WFvz3T0TcrOqwLX1M/DQvcHaGGw0B0y4bZMs7lVScGBFxMj3vbFi2SRKbKhaitxHfYHAOAa0X7/MSS0RNAjdwoyGHeOepXOKY+h3iHeqCvgOH6LOifdHf/1aaZNwSkGotYnYScW8Yx63LnSwba7+hESrtPa/huRmB9KWvMCKbDThL/nne14hnL277EDCSocPu3rOSYjuB9gKSOdVmWsj9Dxb/iZIe+S6AiG29Esm+/eUacSba0k8wn5HhHg9d4tIcixrxveflc8vi2/wNQGVFNsGO6tB5WF0xf/plngOvQ1/ivGV/C1Qpdhzznh0ExAVJ6dwzNg7qIEBaw+BzTJTUuRcPk92Sn6QDn2Pu3mpONaEumacjW4w6ipPnPw+g2TfywJjeEcpSZaP4Q3YV5HG8D6UjWA4GSkBKculWpdCMadx0usMomsSS/74QgpYqcPkmamB4nVv1JxczYITIqItIKjD35IGKAUwAA==",
              "Content-Type": "application/x-www-form-urlencoded",
            }
            var loginobj = {
              url: redirecturl + "&fun=new",
              header: header,
              success: function (logininfo) {
                if (logininfo.code == 200) {
                  var matchObj = MatchLoginResult(logininfo.data)
                  if (matchObj.ret == 0) {
                    for (var element in matchObj) {
                      if (["skey", "wxsid", "wxuin", "pass_ticket"].indexOf(element) >= 0) {
                        localStorage.setItem(("wx." + element), matchObj[element], "com.github.wx")
                      }
                    }

                    LOCAL_SKEY_ = localStorage.getItem("wx.skey")
                    LOCAL_WXSID = localStorage.getItem("wx.wxsid")
                    LOCAL_WXUIN = localStorage.getItem("wx.wxuin")
                    LOCAL_PSTKT = localStorage.getItem("wx.pass_ticket")

                    if (logininfo.response || logininfo.headers) {
                      var getCookie = logininfo.response ? logininfo.response : logininfo.headers
                      getCookie = getCookie["Set-Cookie"]
                      if (getCookie) {
                        COOKIES = getCookie
                        localStorage.setItem("wx.cookie", getCookie, "com.github.wx")
                        DOMAIN = GetValueBtKey(getCookie, "Domain")
                        localStorage.setItem("wx.domain", DOMAIN, "com.github.wx")
                        console.log("#>> DOMAIN: " + DOMAIN);
                      }
                    }
                    if (PAGEOBJECT["login"]) {
                      PAGEOBJECT["login"].loginSucc();
                    }
                  } else {
                    // 登陆失败场景
                    if (PAGEOBJECT["login"]) {
                      PAGEOBJECT["login"].loginFail();
                    }
                  }
                }
              }
            }
            getRequest(loginobj)
          }
        } else {
          if (scandtstus == 201) {
            if (!PAGEOBJECT["login"]) {
              return null
            }
            PAGEOBJECT["login"].setStatus("scaned")
          }
          getLogin_Status()
        }
      }
    },
    fail: function (err) {
      console.log('#>> get_qrcode_status fail err---', err);
    },
    complete: function (flag) {
      console.log("#> complete complateflag: ");
    }
  }
  getRequest(obj)
}

function initWechat() {
  var url = "https://" + DOMAIN + "/cgi-bin/mmwebwx-bin/webwxinit"
  var params = { _: new Date().getTime(), }
  var header = { Cookie: COOKIES, 'Content-Type': 'application/json', }

  var obj = {
    url: url,
    params: params,
    header: header,
    responseType: "json",
    success: function (initRes) {
      var data = initRes.data
      if (data && data.BaseResponse && data.BaseResponse["Ret"] == 0) {
        localStorage.setItem("wx.servered", 1, "com.github.wx")
        var nickname = data.User.NickName;
        if (PAGEOBJECT["main"]) {
          PAGEOBJECT["main"].setNickName(nickname)
        }
        USERNAME = data.User.UserName
        localStorage.setItem("wx.username", USERNAME, "com.github.wx")

        SYNCKEY = data.SyncKey
        localStorage.setItem("wx.synckey", SYNCKEY, "com.github.wx")

        if (data.ContactList) {
          var forlen = 10;
          if (data.ContactList.length < forlen) {
            forlen = data.ContactList.length
          }

          // for (var index = 0; index < data.ContactList.length; index++) {
          for (var index = 0; index < forlen; index++) {
            var element = data.ContactList[index];
            // @ 联系人 @@ 群
            if (element.UserName[0] != "@") {
              console.log("#>> ignore wx name: " + element.UserName);
              continue
            }
            if (element.UserName.slice(0, 2) == "@@") {
              LOOPGETGROUPIDS.push(element.UserName)
            }
            GETWECHATLIST.push({
              count: 0,
              name: (element.RemarkName ? element.RemarkName : element.NickName),
              message: [],
              lastmessage: "@nomsgyet",
              time: new Date().getTime(),
              userid: element.UserName,
            })
          }
          GetGroupInfoAndUName(LOOPGETGROUPIDS.shift())
        }
      }
      if (PAGEOBJECT["main"]) {
        console.log("#>>> main set msg list");
        PAGEOBJECT["main"].syncView()
        SyncCheck()
      }
    }
  }
  postRequest(obj)
}

function SyncCheck() {
  var url = "https://" + DOMAIN + "/cgi-bin/mmwebwx-bin/synccheck"
  var header = {
    cookie: COOKIES,
    'Content-Type': 'application/json',
  }
  var time = new Date().getTime()
  var params = {
    r: time,
    skey: LOCAL_SKEY_,  // localStorage.getItem("wx.skey"),
    sid: LOCAL_WXSID,   // localStorage.getItem("wx.wxsid"),
    uin: LOCAL_WXUIN,   // localStorage.getItem("wx.wxuin"),
    deviceid: DEVICEID,
    _: time,
    synckey: syncKeyToString(SYNCKEY),
  }

  var paramsStr = jsonSort(params)
  if (paramsStr) { url += "?" + paramsStr }

  var obj = {
    url: url,
    header: header,
    success: function (res) {
      if (res && res.code == 200) {
        var data = res.data
        var val = GetValueBtKey(data, 'window.synccheck').slice(1, -1)
        var _ = val.split(',')[0]
        var __ = _.split(':')[1].trim()
        console.log("#>> wx sync check:" + data, __ + " ___ " + (__ == "0") + "  active: " + APPACTIVE);
        if (APPACTIVE) {
          if (__ == "0") {
            SyncMessage()
          } else {
            if (PAGEOBJECT["main"]) {
              console.log("#>>> relogin");
              PAGEOBJECT["main"].reLogin()
            }
          }
        }
      }
    },
  }

  getRequest(obj)
}

function SyncMessage() {
  var syncurl = "https://" + DOMAIN + "/cgi-bin/mmwebwx-bin/webwxsync"
  var ayncparams = {
    skey: LOCAL_SKEY_,        // localStorage.getItem("wx.skey"),
    sid: LOCAL_WXSID,         // localStorage.getItem("wx.wxsid"),
    pass_ticket: LOCAL_PSTKT, // localStorage.getItem("wx.pass_ticket"),
    rr: new Date().getTime(),
  }
  var header = { cookie: COOKIES, 'Content-Type': 'application/json', }
  var syncdata = { SyncKey: SYNCKEY }

  var syncparamsStr = jsonSort(ayncparams)
  if (syncparamsStr) { syncurl += "?" + syncparamsStr }
  var objx = {
    url: syncurl,
    header: header,
    data: syncdata,
    responseType: "json",
    success: function (syncres) {
      var resdata = syncres.data
      if (resdata.SyncKey.Count > 0) {
        SYNCKEY = resdata.SyncKey
        localStorage.setItem("wx.synckey", SYNCKEY, "com.github.wx")
        console.log("#>> SyncKey reset");
      }
      if (resdata.SyncCheckKey.Count > 0) {
        console.log("#>> SyncCheckKey reset");
        SYNCCHECKKEY = resdata.SyncCheckKey
      }

      var forlen = 10;
      if (resdata.AddMsgList.length < forlen) {
        forlen = resdata.AddMsgList.length
      }

      for (var index = 0; index < forlen; index++) {
        var item = resdata.AddMsgList[index];
        if (item.MsgType === 1) {
          var element = null
          var matchExisted = false

          var frommsgid = item.FromUserName
          var tomsgid = item.ToUserName
          var agroupId = "";
          if (frommsgid.slice(0, 2) == "@@") {
            agroupId = frommsgid
          } else if (tomsgid.slice(0, 2) == "@@") {
            agroupId = tomsgid
          }
          delete item.RecommendInfo
          delete item.AppInfo
          console.log("#>>> text msg: " + agroupId, item);

          var fetchId = ""
          var notifiStr = ""
          if (agroupId) {
            var uID = item.Content.slice(0, item.Content.indexOf(":"))
            var idObj = GROUPIDANDMEMBERLIST[agroupId]
            console.log("#>> id:[" + agroupId + "] is group list. content:" + item.Content)//, idObj);
            if (idObj && idObj["Number"]) {
              var uName = idObj["Number"][uID]
              if (uName) {
                // console.log(`item content: ${item.Content} uid: ${uID} uname: ${uName}`);
                item.Content = item.Content.replace(uID, uName).replace(/<br\/>/g, "\r\n")
              }
              console.log("#>> in group, text: " + item.Content);
              if (false && idObj["name"]) {
                notifiStr = idObj["name"] + ":\r\n"
              }
            } else {
              console.log("#>> not in group, text: " + item.Content);
              fetchId = agroupId
            }
          } else {
            var fidObj = GROUPIDANDMEMBERLIST[frommsgid]
            console.log("#>> user id: " + frommsgid + "  content:" + item.Content)//, fidObj);
            if (!fidObj) {
              fetchId = frommsgid
            }
            if (!GROUPIDANDMEMBERLIST[tomsgid]) {
              fetchId = tomsgid
            }
            if (false && fidObj && fidObj["name"]) {
              notifiStr = fidObj["name"] + ":"
            }
          }

          if (fetchId) {
            // GetGroupInfoAndUName(fetchId)
            LOOPGETGROUPIDS.push(fetchId)
          }

          var sendIsMe = (frommsgid == USERNAME)
          for (var idx = 0; idx < GETWECHATLIST.length; idx++) {
            element = GETWECHATLIST[idx];
            if (element.userid == agroupId || element.userid == frommsgid) {
              matchExisted = true
              if (!sendIsMe) {
                element.count += 1;
              }
              element.message.push({ type: 1, msgid: item.MsgId, content: item.Content, msgtype: (sendIsMe ? "send" : "receive") })
              notifiStr += element.lastmessage = item.Content.replace(/\r\n/g, "")
              element.time = new Date().getTime()
            }
          }

          if (!matchExisted) {
            var fmtCtnt = item.Content.replace(/\r\n/g, "")
            notifiStr += fmtCtnt
            var cccnt = 1
            if (sendIsMe) {
              cccnt = 0
            }
            GETWECHATLIST.unshift({
              count: cccnt,
              name: "undefined",// (element.RemarkName ? element.RemarkName : element.NickName),
              message: [{ type: 1, msgid: item.MsgId, content: item.Content, msgtype: (sendIsMe ? "send" : "receive") }],
              lastmessage: fmtCtnt,
              time: new Date().getTime(),
              userid: (agroupId ? agroupId : frommsgid),
            })
          }

          if (app.getInfo().id != "com.github.wx" && frommsgid != USERNAME) {
            var message = {
              id: "1",
              title: "WX",
              msg_type: "TextMsg", // "TextMsg",
              notification_way: "3", // "ShockAndBright",
              icon_path: "5",
              // sender: "app://com.github.wx#pages/mainWindow/mainWindow",
              sender: "wechat",
              image_context_path: "y",
              text_content: notifiStr,
              priority: 0,
              arrived_time: 0,
            }
            var resu = notification.pushMsgSync(message)
            console.log("#>> notification message: " + resu, message);
          }

          GETWECHATLIST.sort(function (a, b) {
            var timeA = new Date(a.time);
            var timeB = new Date(b.time);
            // 比较时间，倒序排列
            return timeB - timeA;
          });

          if (GETWECHATLIST.length > 10) {
            GETWECHATLIST.pop()
          }
          if (PAGEOBJECT["main"]) {
            console.log("#>>> main set msg list " + new Date().toString());
            PAGEOBJECT["main"].syncView()
          }

          var chatpage = PAGEOBJECT["chat"]
          // if (chatpage && (chatpage.toUserName == frommsgid || frommsgid == USERNAME)) {
          if (chatpage && chatpage.toUserName == frommsgid) {
            chatpage.appendMsg({ type: 1, msgid: item.MsgId, content: item.Content, msgtype: "receive" })
            for (var ix = 0; ix < GETWECHATLIST.length; ix++) {
              var ele = GETWECHATLIST[ix];
              if (ele.userid == agroupId || ele.userid == frommsgid) {
                ele.count = 0;
                GETWECHATLIST[ix].count = 0
              }
            }
          }
          console.log("#>>> check 2 check - start1111");
        } else if (item.MsgType === 3) {
          // 判断 message id，将消息推送到 GETWECHATLIST 的消息列表内
          // getMedia('/cgi-bin/mmwebwx-bin/webwxgetmsgimg', item.MsgId, item.MsgId + ".jpg")
          // uploadMedia(path.resolve(__dirname, '../assert/01_img.jpg'), item.FromUserName, item.ToUserName)
        } else if (item.MsgType === 34) {
          // getMedia('/cgi-bin/mmwebwx-bin/webwxgetvoice', item.MsgId, item.MsgId + ".mp3")
        }
      }
      GetGroupInfoAndUName(LOOPGETGROUPIDS.shift())
      SyncCheck()
    },
    fail: function (params) {
      console.log("#>>> check 2 check - start2222");
      SyncCheck()
    },
    complete: function () {
      console.log("#>>> 5yncCheck complete");
    }
  }
  postRequest(objx)
}

function GetGroupInfoAndUName(gID) {
  if (!gID) {
    console.log("#>> GetGroupInf0AndUName gID is undefined, return");
    return
  }
  var url = "https://" + DOMAIN + "/cgi-bin/mmwebwx-bin/webwxbatchgetcontact"

  var params = { pass_ticket: LOCAL_PSTKT, type: "ex", }
  var paramsStr = jsonSort(params)
  if (paramsStr) { url += "?" + paramsStr }
  var list = [{ "UserName": gID, "EncryChatRoomId": "" },]
  var data = { List: list, Count: list.length, }
  var header = { Cookie: COOKIES, 'Content-Type': 'application/json', }
  var obj = {
    url: url,
    header: header,
    data: data,
    responseType: "json",
    success: function (infoRes) {
      if (infoRes.code == 200) {
        var infoData = infoRes.data
        if (infoData.BaseResponse && infoData.BaseResponse.Ret == 0) {
          var getCont = infoData.ContactList[0]
          var groupmembers = getCont.MemberList
          var asuser = false
          if (gID.slice(0, 2) == "@@") {
            GROUPIDANDMEMBERLIST[gID] = { name: getCont.NickName, Number: {} }
          } else {
            GROUPIDANDMEMBERLIST[gID] = { name: getCont.NickName }
            asuser = true
          }

          for (var index = 0; index < groupmembers.length; index++) {
            var element = groupmembers[index];
            // console.log(index + "# >> groupmembers: ", element);
            GROUPIDANDMEMBERLIST[gID]["Number"][element.UserName] =
              (element.DisplayName ? element.DisplayName : element.NickName)

            if (GROUPIDANDMEMBERLIST[element.UserName] == "") {
              GROUPIDANDMEMBERLIST[element.UserName] = { name: element.NickName }
              console.log(element.UserName + "#>>>>> set user nickname: " + element.NickName);
            }
          }

          for (var idx = 0; idx < GETWECHATLIST.length; idx++) {
            var ele = GETWECHATLIST[idx];
            if (ele.userid == gID) {
              console.log("#>>>> get group name in chat ") // , ele);
              if (ele.name == "undefined") {
                ele.name = (getCont.DisplayName ? getCont.DisplayName : getCont.NickName)
                GETWECHATLIST[idx].name = ele.name
                console.log("#>> group name is undef, set new name: " + ele.name);
              }

              var ncon = ""
              for (var _msg = 0; _msg < ele.message.length; _msg++) {
                var msg1 = ele.message[_msg];
                // console.log(_msg + " #>>>> msg1.content: " + msg1.content, GROUPIDANDMEMBERLIST[gID]);
                if (msg1 && msg1.content) {
                  var uid = msg1.content.slice(0, msg1.content.indexOf(":"))
                  if (asuser) {
                    ncon = msg1.content
                    GETWECHATLIST[idx].message[_msg].content = ncon
                  } else if (GROUPIDANDMEMBERLIST[gID]["Number"][uid]) {
                    // var uid = msg1.content.slice(0, msg1.content.indexOf(":"))
                    var repStr = GROUPIDANDMEMBERLIST[gID]["Number"][uid]
                    ncon = msg1.content.replace(uid, repStr).replace(/<br\/>/g, "\r\n")
                    GETWECHATLIST[idx].message[_msg].content = ncon
                    console.log("#>> getname and replace: [" + uid + "-->" + repStr + "] eeeeeeeeend " + ncon);
                  }
                }
              }
              GETWECHATLIST[gID] = ele
              GETWECHATLIST[gID].lastmessage = ncon.replace(/<br\/>/g, "\r\n").replace("\r\n", "")
              break
            }
          }
          // console.log("GROUPIDANDMEMBERL1ST>>>>> ", GROUPIDANDMEMBERLIST);
          if (PAGEOBJECT["main"]) {
            console.log("#>>> getGroupName refresh msg list " + new Date().toString());
            PAGEOBJECT["main"].syncView()
          }
        }
      }
    },
    complete: function () {
      GetGroupInfoAndUName(LOOPGETGROUPIDS.shift())
    }
  }
  postRequest(obj)
}

function SendMsg(msg, toUser, msid) {
  var url = "https://" + DOMAIN + "/cgi-bin/mmwebwx-bin/webwxsendmsg"
  var s = new Date().getTime() + GetRandomString(6)
  var relpyMsg = {
    Type: '1',
    Content: msg,
    FromUserName: USERNAME,
    ToUserName: toUser,
    LocalID: s,
    ClientMsgId: s,
    MediaId: '',
  }

  var data = { Msg: relpyMsg, Scene: 0, }
  // var params = { lang: "zh_CN", pass_ticket: localStorage.getItem("wx.pass_ticket") }
  var params = { lang: "zh_CN", pass_ticket: LOCAL_PSTKT }
  var header = { Cookie: COOKIES, 'Content-Type': 'application/json', }

  var paramsStr = jsonSort(params)
  if (paramsStr) { url += "?" + paramsStr }

  var obj = {
    url: url,
    header: header,
    data: data,
    responseType: "json",
    success: function (sendres) {
      if (sendres.code == 200) {
        var sendresdata = sendres.data
        if (sendresdata.BaseResponse.Ret == 0) {
          var element = null
          var frommsgid = toUser
          for (var idx = 0; idx < GETWECHATLIST.length; idx++) {
            element = GETWECHATLIST[idx];
            if (element.userid == frommsgid) {
              element.count = 0;
              element.message.push({ type: 1, msgid: sendresdata.MsgID, content: relpyMsg.Content, msgtype: "send" })
              element.lastmessage = relpyMsg.Content
              element.time = new Date().getTime()
              console.log("#>>> get new date: " + element.time);

              GETWECHATLIST.sort(function (a, b) {
                var timeA = new Date(a.time);
                var timeB = new Date(b.time);
                // 比较时间，倒序排列
                return timeB - timeA;
              });

              if (PAGEOBJECT["main"]) {
                PAGEOBJECT["main"].syncView()
              }

              var chatpage = PAGEOBJECT["chat"]
              if (chatpage && chatpage.toUserName == frommsgid) {
                chatpage.sendSuccId(msid)
              }
            }
          }
        }
      }
    }
  }

  postRequest(obj)
}

function GetRandomString(len) {
  var symbols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  var builder = []
  for (var i = 0; i < len; i++) {
    var r = Math.floor(Math.random() * symbols.length);
    builder.push(symbols[r]);
  }
  return builder.join("")
}

function getMedia(fetchurl, msgId, fileName) {
  var url = "https://" + DOMAIN + fetchurl
  var headers = { Cookie: COOKIES, }
  // var params = { MsgID: msgId, skey: localStorage.getItem("wx.skey"), }
  var params = { MsgID: msgId, skey: LOCAL_SKEY_, }
  var retype = 'arraybuffer'

  var mediaparams = jsonSort(params)
  if (mediaparams) { url += "?" + mediaparams }

  var obj = {
    url: url,
    headers: headers,
    retype: retype,
    params: params,
    success: function (mediares) {

      console.log("#>>>> get mediares", mediares);

      if (mediares && mediares.code == 200) {
        // MEDIAURL
        var writefile = (MEDIAURL + fileName)
        var writeFileSync = FileSystemManager.writeFileSync({
          filePath: writefile,
          data: mediares.data,
        });
        console.log("writeFileSync:", writeFileSync);

        // await file.write({
        //   uri: writefile,
        //   data: mediares.data,
        //   success: () => {
        //     console.log(`#>> file [${writefile}] write success`);
        //   },
        // })
      }
    }
  }
  getRequest(obj)
}

function syncKeyToString(key) {
  var list = key.List

  var appendLst = []
  if (list) {
    for (var index = 0; index < list.length; index++) {
      var element = list[index];
      appendLst.push(element.Key + "_" + element.Val)
    }
  }

  return appendLst.join('|')
}

function MatchLoginResult(xmlString) {
  // 定义正则表达式模式匹配标签及其文本内容
  var tagPattern = /<([^>]+)>([^<]*)<\/\1>/g;

  // 定义一个对象用于存储解析后的数据
  var parsedData = {};

  // 匹配标签及其内容并存储到对象中
  var match;
  while ((match = tagPattern.exec(xmlString)) !== null) {
    var key = match[1];
    var value = match[2];
    parsedData[key] = value;
  }
  return parsedData
}

// #region obj2string、promise
function GetValueBtKey(data, getkey) {
  var oprtionObj = {}
  var colonArr = data.split(";")
  for (var idx = 0; idx < colonArr.length; idx++) {
    var element = colonArr[idx];
    while (element[0] == " ") {
      element = element.slice(1)
    }

    while (element[element.length - 1] == " ") {
      element = element.slice(0, -1)
    }

    var index = element.indexOf("=")
    var key = element.slice(0, index).trim()
    var val = element.slice(index + 1).trim()
    if (key && val) {
      oprtionObj[key] = val.replace(/"/g, "");
    }
  };
  return oprtionObj[getkey]
}

function jsonSort(jsonObj) {
  var arrKeys = []
  for (var key in jsonObj) {
    arrKeys.push(key)
  }
  var str = ''
  for (var i in arrKeys) {
    str += arrKeys[i] + '=' + jsonObj[arrKeys[i]] + '&'
  }
  str = str.slice(0, -1)
  return str
}
// #endregion

function GetStringLength(str) {
  var strLen = str.replace(/[^\u0000-\u00FF]/g, '**').length;
  return strLen;
}

function SyncPageObject(pageKey, pageObj) {
  PAGEOBJECT[pageKey] = pageObj;
}

function GetChatItems() {
  return GETWECHATLIST
}

function GetAppActive() {
  return APPACTIVE
}

function SetAppActive(act) {
  APPACTIVE = act
}

module.exports = {
  initialConfig: initialConfig,
  uninitialConfig: uninitialConfig,
  GetLoginURL: GetLoginURL,
  getLoginStatus: getLogin_Status,
  initWechat: initWechat,
  SyncCheck: SyncCheck,
  SendMsg: SendMsg,
  GETWECHATLIST: GETWECHATLIST,
  ResetGlobalVar: ResetGlobalVar,
  SyncPageObject: SyncPageObject,
  GetAppActive: GetAppActive,
  SetAppActive: SetAppActive,
  GetChatItems: GetChatItems,
}
