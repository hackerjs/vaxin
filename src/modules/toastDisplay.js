var FileSystemManager = pm.getFileSystemManager();

module.exports = {

  show: function (msgStr, time) {
    pm.hideToast();
    var comfirmPath = "internal://system.apps#com.github.wx/app.prc";
    if (FileSystemManager.accessSync({ path: comfirmPath })) {
      comfirmPath = "internal://user.apps#com.github.wx/app.prc";
      if (FileSystemManager.accessSync({ path: comfirmPath })) { }
    }
    // console.log("#>> show toast prcPath: " + comfirmPath);

    if (time == undefined) {
      time = 3000;
    }

    pm.showToast({
      title: pm.getResString(msgStr, comfirmPath),
      duration: time,
      mask: false,
      width: 410,
      hMargin: 30,
      vMargin: 20,
      fgColor: 0xFFFFFFFF,
      bgColor: 0xFF1D1D1D,
      rowHeight: 36,
      font: { family: "/system/fonts/defaultFont", size: 32 }
    });
  },

  getValueByKey: function (msgStr) {
    var comfirmPath = "internal://system.apps#com.github.wx/app.prc";
    if (FileSystemManager.accessSync({ path: comfirmPath })) {
      comfirmPath = "internal://user.apps#com.github.wx/app.prc";
      if (FileSystemManager.accessSync({ path: comfirmPath })) { }
    }
    return pm.getResString(msgStr, comfirmPath);
  },

}