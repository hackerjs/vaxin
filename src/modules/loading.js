var toastDisplay = require("modules/toastDisplay");
var screen = require("system.screen");
var scTimes = 0;
module.exports = {
  loadTimeout: false,
  timeoutTimer: null,

  show: function (page, y, lblStr) {
    this.loadTimeout = false;

    if (!y) {
      y = 80;
    }

    if (!lblStr) {
      lblStr = "@uiloading"
    }

    console.log("++++++++++++++++++++++++++++ " + page._name + "  " + lblStr);

    var obj1 = {}
    obj1[page._name] = {
      panel: {
        items: [{
          xml: 'panels/loading',
          items: [{
            loading: { id: "loading", rect: { y: y, height: 400, } },
            loadingPanelLabel2: lblStr,
            loadingPanelanimatedImage1: { id: "loadingPanelanimatedImage1", value: "start", }
          }]
        }]
      }
    }
    page.setData(obj1)

    this.canvasDisplay(page, true);
  },

  showToast: function (showStr, time) {
    scTimes = screen.getScreenOffTimeSync();
    console.log("#> showToast get screenTime: " + scTimes + "  time: " + (pm.clock() / 1000));
    screen.setScreenOffTime({ time: 7200, }); // 常量 2h
    if (time == undefined) {
      time = 30000;
    }
    toastDisplay.show(showStr, time);
  },

  hideToast: function () {
    if (scTimes) {
      console.log("#> hideToast set screenTime: " + scTimes + "  time: " + (pm.clock() / 1000));
      screen.setScreenOffTime({ time: scTimes });
      scTimes = null;
    }
    pm.hideToast();
  },

  canvasDisplay: function (page, show) {
    if (show) {
      scTimes = screen.getScreenOffTimeSync();
      console.log("#> loading get screenTime: " + scTimes + "  time: " + (pm.clock() / 1000));
      screen.setScreenOffTime({ time: 7200, }); // 常量 2h
    } else {
      if (scTimes) {
        console.log("#> unloading set screenTime: " + scTimes + "  time: " + (pm.clock() / 1000));
        screen.setScreenOffTime({ time: scTimes });
        scTimes = null;
      }
      if (page) {
        var obj = {};
        obj[page._name] = { remove: "loading" };
        page.setData(obj);
      }
    }
  },

  hide: function (page) {
    this.canvasDisplay(page, false);
    this.timeoutTimer && clearInterval(this.timeoutTimer);
  },

  // 超时时间，超时提示
  // 默认十秒后提示【获取失败，请稍后重试】
  timeout: function (page, time, msgStr) {

    if (!time) { time = 15000 * 2; }
    if (!msgStr) { msgStr = "@loadingTimeout"; }

    var that = this;
    this.timeoutTimer = setTimeout(function () {
      that.loadTimeout = true;
      that.hide(page);
      toastDisplay.show(msgStr);
      // pm.navigateBack();
    }, time)
  },
}