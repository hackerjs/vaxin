﻿var transitions = require("transitions");
var KEY_UP = 1;
var KEY_LONG_UP = 4;
var LCD_ON = 1;
var screen = require("system.screen");
var network = require('system.network')
var toastDisplay = require("modules/toastDisplay");
var keypad = require("system.keypad")

App({
  page: "pages/mainWindow/mainWindow",
  data: {
    scroll: { pitch: 5, time: 100 },
    noScroll: { pitch: 0, time: 1000 },
    netType: "none",
    appAbsltDataPath: "/data/apps/com.github.wx",
    appDataURIPath: "internal:///#com.github.wx",

    VoiceKey: "D9RV8Nz0hAwCB5HHZYoLTkGG",
    VoiceSecret: "IgB0bXoslAony1CyITL6UylA6wGRGq6A",
    exitFlag: false,
    backgrounder: true,
  },
  netObj: {},
  noNetTipTimestemp: 0,

  /* app 加载完成触发该函数 */
  onLaunch: function (event) {
    var servered = localStorage.getItem("wx.servered") || 0;
    console.log("#>>>> severd: " + servered);
    if (!servered) {
      this.page = "pages/login/login";
    } else {
      setTimeout(function () {
        var WX = require("modules/wx");
        WX.initialConfig()
      }, 1);
    }

    this.initFileSystem()

    function show2st(showStr) {
      // 无网络提示间隔 60 秒
      var nowTime = new Date().getTime();
      if (showStr == "@loadErrCheckNet" && (nowTime - that.noNetTipTimestemp) < 60000) {
        console.log("#>> loadErrCheckNet tips less than 1min, return");
        return;
      }
      // 更新本次提醒时间
      that.noNetTipTimestemp = nowTime;
      toastDisplay.show(showStr);
    }

    var that = this;
    network.getType({
      success: function (data) {
        console.log("network get type success: " + data.type);
        that.data.netType = data.type;
        localStorage.setItem("wx.nettype", data.type, "com.github.wx");
        if (data.type == "none") {
          show2st("@noNetCheckLink");
        }
      },
      fail: function () {
        console.log("# network get type fail");
      },
      complete: function () {
        console.log("# network get type complete");
      },
    });

    this.netObj = {
      callback: function (data) {
        console.log("# network.subscribe>> " + data.type);
        that.data.netType = data.type;
        localStorage.setItem("wx.nettype", data.type, "com.github.wx");
        if (data.type == "none") {
          show2st("@noNetCheckLink");
        }
      },
      fail: function () { }
    }

    // 设置监听
    network.subscribe(this.netObj)

    var saveAudio = require("modules/saveAudio")
    saveAudio.Save()
  },

  initFileSystem: function () {
    var FileSystemManager = pm.getFileSystemManager();
    var that = this;
    var array = ["useravator", "login",]
    for (var i = 0; i < array.length; i++) {
      FileSystemManager.mkdir({
        dirPath: that.data.appDataURIPath + "/images/" + array[i],
        recursive: true,
      });
    }

    var files = ["media", "voice",]
    for (var index = 0; index < files.length; index++) {
      var ele = files[index];
      var mkdirSync = FileSystemManager.mkdirSync({
        dirPath: "internal://files#" + ele,
        recursive: true,
      });
    }
  },

  /* app 可见时触发该函数 */
  onShow: function (event) {
    var that = this;
    function powerCallback(args) {
      console.log("type: " + (args.type ? "func key" : "power key") + "  value: " + (args.value > 3 ? "long up" : args.value > 2 ? "longpress" : args.value > 1 ? "press" : "up"));
      if (args.type == 1) { return } // func key
      var event = args.value;
      var screenStatus = screen.getStatusSync();
      var pageName = pm.getPageName();
      if (event == KEY_UP || event == KEY_LONG_UP) {
        if (screenStatus == LCD_ON) {
          if (pm.floatPageCount() >= 1) return;
          console.log("#>>> appjs - back pagename: " + pageName);
          if (pageName == "mainWindow") {
            transitions.applist2App();
            var appidd = "com.github.wx";
            if (that.data.netType == "none") {
              console.log("#>> app.js exit, net is none, playing is false.");
              app.exit();
              return;
            } else {
              console.log("#>> backgrounder: " + app.getInstance().data.backgrounder);
              if (that.data.backgrounder) {
                app.hide();
              } else {
                pm.navigateBack()
              }
            }
          } else {
            if (app.getInfo().id == "com.github.wx") {
              transitions.appOrPageEnterAnim();
              pm.navigateBack()
            }
          }
        }
      }
    }

    keypad.subscribe(this.powerKeyCallback = { callback: powerCallback });
  },

  /* app 不可见时触发该函数 */
  onHide: function (event) {
    keypad.unsubscribe(this.powerKeyCallback)
  },

  /* app 退出触发该函数 */
  onExit: function (event) {
    // 取消监听
    console.log("#>> wx app exit. " + (new Date()));
    network && network.unsubscribe(this.netObj)
  },
});